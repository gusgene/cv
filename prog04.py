import cv2


def main():
    imgpath = "Dataset\\4.1.03.tiff"
    img = cv2.imread(imgpath)

    outpath = "Output\\4.1.03.jpg"


    cv2.imshow('Lena', img)
    cv2.imwrite(outpath, img)
    cv2.waitKey(0)
    cv2.destroyWindow('Lena')


if __name__ == "__main__":
    main()