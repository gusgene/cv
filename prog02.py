import cv2


def main():
    imgpath = "Dataset\\4.1.03.tiff"
    img = cv2.imread(imgpath)

    cv2.imshow('LENA', img)
    cv2.waitKey(0)
    cv2.destroyAllWindows()


if __name__ == "__main__":
    main()